package com.example.dimapp

import android.os.AsyncTask
import android.util.Log
import okhttp3.*
import java.io.IOException

class APIService {
    var client = OkHttpClient();

    @Throws(IOException::class)
    fun run(url:String) {
        val request = Request.Builder()
            .url(url)
            .build()
        client.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                e.printStackTrace()
            }

            override fun onResponse(call: Call, response: Response) {
                response.use {
                    if (!response.isSuccessful) throw IOException("Unexpected code $response")

                    for ((name, value) in response.headers) {
                        Log.d("DEBUG-27", "$name: $value")
                    }

                    Log.d("DEBUG-27", response.body!!.string())
                }
            }
        })
    }
}


